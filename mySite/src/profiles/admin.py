from django.contrib import admin

from .models import CustomUser
from .models import product

class CustomUserAdmin(admin.ModelAdmin):
	class Meta:
		model = CustomUser

admin.site.register(CustomUser, CustomUserAdmin)


class productAdmin(admin.ModelAdmin):
	class Meta:
		model = product

admin.site.register(product, productAdmin)