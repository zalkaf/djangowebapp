from django import forms
from models import product, CustomUser
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class productForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     self.user = kwargs.pop('user', None)
    #     super(productForm, self).__init__(*args, **kwargs)

    class Meta:
        model = product

        #email = forms.EmailField(label='Email', max_length=256, widget=forms.HiddenInput())
        product_id = forms.CharField(label='Product ID', max_length=90)
        product_name = forms.CharField(label='Product Name', max_length=150)
        product_desc = forms.CharField(label='Product Description', widget=forms.Textarea)
        locations = (
            ('Olaya', 'Olaya'),
            ('King Fahad', 'King Fahad'),
            ('King Abdulla', 'King Abdulla'),
            ('Takhasusi', 'Takhasusi'),
            ('Sulimaniyah', 'Sulimaniyah'),
            ('Al-Nafl', 'Al-Nafl'),
            ('Al-Falah', 'Al-Falah'),
            ('Al-Malaz', 'Al-Malaz'),
            ('Rawda', 'Rawda'),
            ('Al-Waha', 'Al-Waha'),
        )  

        dropoff_location = forms.ChoiceField(
            choices = locations,
            label = 'Dropoff Location'
        )

        #fields = ['product_id', 'product_name', 'product_desc', 'dropoff_location']
        exclude = ['email']


class CustomUserCreationForm(UserCreationForm):
    # """
    # A form that creates a user, with no privileges, from the given email and
    # password.
    # """

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('email', 'first_name','last_name', 
            'birth_date', 'instagram_id', 'my_location')
