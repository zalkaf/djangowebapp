from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import CustomUser, product
from forms import productForm, CustomUserCreationForm
from django.http import HttpResponseRedirect
from django.template.context_processors import csrf
from django.contrib import auth


# Create your views here.

def home(request):
	context=locals()
	template='home.html'
	return render(request, template, context)


def about(request):
	context=locals()
	template='about.html'
	return render(request, template, context)

def contact(request):
	context=locals()
	template='contact.html'
	return render(request, template, context)

def auth_view(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = auth.authenticate(username=username, password=password)
    
	if user is not None:
		print ("user is not none....")
		auth.login(request, user)
		return HttpResponseRedirect('/account/loggedin/')
	else:
		return HttpResponseRedirect('/account/invalid_login')
    
def login(request):
	c = {}
	c.update(csrf(request))    
	return render(request, 'account/login.html', c)

def invalid_login(request):
	return render(request, 'account/invalid_login.html')

def loggedin(request):
	return render(request, 'account/loggedin.html')

def logout(request):
	auth.logout(request)
	return render(request, 'account/logout.html')

def register_user(request):
	if request.method == 'POST':
		print "request.method = POST"
		form = CustomUserCreationForm(request.POST)
		if form.is_valid():
			print "form is valid"
			form.save()
			return HttpResponseRedirect('/account/register_success')
        
	else:
		print "request.method is NOT POST"
		form = CustomUserCreationForm()
	args = {}
	args.update(csrf(request))
    
	args['form'] = form
    
	return render(request, 'account/register.html', args)



def register_success(request):
	return render(request, 'account/register_success.html')



@login_required
def productView(request):
	if request.POST:
		form = productForm(request.POST)

		candidate = form.save(commit=False)
		candidate.email = CustomUser.objects.get(email=request.user.email)  # use your own profile here

		if form.is_valid():
			print "form is valid"
			candidate.save()

			return HttpResponseRedirect('/')
		else:
			print "form is NOT Valid....."

	else:
		form = productForm()

	args = {}
	args.update(csrf(request))

	args['form'] = form
	args['user'] = request.user.email

	#print "args", request.user.email

	return render_to_response("add_product.html", args)


@login_required
def list_products(request):
	queryset = product.objects.filter(email=request.user)
	context = {
		'object_list' : queryset,
		'user' : request.user
	}
	return render(request, 'view_products.html', context)





