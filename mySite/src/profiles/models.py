from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from datetime import datetime    
from django.conf import settings

#### Extending User Model ####


class customUserManager(BaseUserManager):
	def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
			"""
			Creates and saves a User with the given email and password.
			"""
			if not email:
				raise ValueError('The given email must be set')
			time_now = datetime.now()
			email = self.normalize_email(email)
			user = self.model(email=email, 
        	is_staff=is_staff,
        	is_active=True,
        	is_superuser=is_superuser,
        	last_login=time_now,
        	date_joined=time_now,
        	**extra_fields)
        
			user.set_password(password)
			user.save(using=self._db)
			return user

	def create_user(self, email, password=None, **extra_fields):
				return self._create_user(email, password, False, False, **extra_fields)

	def create_superuser(self, email, password, **extra_fields):
				return self._create_user(email, password, True, True, **extra_fields)



class CustomUser(AbstractBaseUser, PermissionsMixin):
	email = models.CharField(max_length=120, default='example@example.com', unique=True)
	username = models.CharField(max_length=254, unique=True, default='username')
	first_name = models.CharField(max_length=100, default='John')
	last_name = models.CharField(max_length=100, default='Mike')
	birth_date = models.DateField(null=True, blank=True, default='1991-05-13')
	instagram_id = models.CharField(max_length=120, default='@hello123')
	locations = (
        ('Olaya', 'Olaya'),
       	('King Fahad', 'King Fahad'),
       	('King Abdulla', 'King Abdulla'),
       	('Takhasusi', 'Takhasusi'),
       	('Sulimaniyah', 'Sulimaniyah'),
       	('Al-Nafl', 'Al-Nafl'),
       	('Al-Falah', 'Al-Falah'),
       	('Al-Malaz', 'Al-Malaz'),
       	('Rawda', 'Rawda'),
       	('Al-Waha', 'Al-Waha'),
    )

	my_location = models.CharField(
	       choices = locations,
       	default = 'Olaya',
       	max_length = 120
  ) 
	
	date_joined  = models.DateTimeField(_('date joined'), auto_now_add=True)
	is_active    = models.BooleanField(default=True)
	is_admin     = models.BooleanField(default=False)
	is_staff     = models.BooleanField(default=False)
	is_superuser = models.BooleanField(default=False)
	

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['instagram_id', 'my_location']
	objects = customUserManager()

	class Meta: 
		verbose_name = _('user') 
		verbose_name_plural = _('users')
		#swappable = 'AUTH_USER_MODEL'

	def get_full_name(self):
		'''
		Returns the first_name plus the last_name, with a space in between.
		'''
		full_name = '%s %s' % (self.first_name, self.last_name)
		return full_name.strip()

	def get_short_name(self):
		'''
		Returns the short name for the user.
		'''
		return self.first_name

	def email_user(self, subject, message, from_email=None, **kwargs):
		'''
		Sends an email to this User.
		'''
		send_mail(subject, message, from_email, [self.email], **kwargs)


class product(models.Model):
	email = models.ForeignKey(CustomUser, default='example@example.com')
	product_id = models.CharField(default='Product ID', max_length=90)		
	product_name = models.CharField(default='Product Name', max_length=150)
	product_desc = models.TextField(default='Product Description', max_length = 1500)

	locations = (
		('Olaya', 'Olaya'),
       	('King Fahad', 'King Fahad'),
       	('King Abdulla', 'King Abdulla'),
       	('Takhasusi', 'Takhasusi'),
       	('Sulimaniyah', 'Sulimaniyah'),
       	('Al-Nafl', 'Al-Nafl'),
       	('Al-Falah', 'Al-Falah'),
       	('Al-Malaz', 'Al-Malaz'),
       	('Rawda', 'Rawda'),
       	('Al-Waha', 'Al-Waha'),
    )

	dropoff_location = models.CharField(
		choices = locations,
		default = 'Olaya',
		max_length = 120
		)
	def __str__(self):
		return self.product_name;
