"""mySite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static 
from django.contrib import admin
from profiles import views as profiles_views
from django.conf.urls import url, include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', profiles_views.home, name='home'),
    #url(r'^accounts/', include('allauth.urls')),
    url(r'^about', profiles_views.about, name='about'),
    url(r'^contact', profiles_views.contact, name='contact'),
    url(r'^add_product', profiles_views.productView, name='add_product'),
    url(r'^view_products', profiles_views.list_products, name='view_products'),
    url(r'^account/login/',  profiles_views.login, name='account/login/'),
    url(r'^account/auth',  profiles_views.auth_view, name='account/auth'),    
    url(r'^account/logout/', profiles_views.logout, name='account/logout'),
    url(r'^account/loggedin/', profiles_views.loggedin ),
    url(r'^account/invalid_login/', profiles_views.invalid_login, name='account/invalid_login'),    
    url(r'^account/register_user/', profiles_views.register_user, name='account/register_user/'),
    url(r'^account/register_success/', profiles_views.register_success, name='account/register_success'),

]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    